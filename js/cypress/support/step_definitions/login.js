import { login } from './util'

/// <reference types="Cypress" />

given("the user is on the website", () => {
  cy.visit('https://sprinkle-burn.glitch.me/')
  cy.get('.header > h1').should('contain', 'Worlds Best App')
});


when("the user enters username {string}", (username) => {
  cy.get('[name=email]').type(username)
});

then("the user enters password {string}", (password) => {
  cy.get('[name=password]').type(password)
});

then("the user is presented with a welcome message", () => {
  cy.contains('Welcome Dr I Test').should('be.visible')
});

then("clicks Login", () => {
  cy.get('.f5').click()
});

//Refactored: using login function & variables
when("the user logs in with {string} and {string}", (username, password) => {
  login(username, password)
})

//Chained assertion validating text and background colour
then("the user is presented with a error message", () => {
  cy.get('#login-error-box').should('have.css', 'background-color', 'rgb(255, 65, 54)')
    .and('contain', 'Credentials are incorrect')
})

//reusable validation looking for error text variable
then("the user is presented with the {string}", (message) => {
  cy.contains(message).should('be.visible')

})


