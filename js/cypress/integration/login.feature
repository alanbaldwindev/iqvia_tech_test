Feature: Login
  In order to use the app the user must be able to Login

  #Added Variables
  Scenario: Login Success
    Given the user is on the website
    When the user enters username "test@drugdev.com"
    And the user enters password "supers3cret"
    And clicks Login
    Then the user is presented with a welcome message

  #Added Variables
  Scenario: Login Incorrect credentials
    Given the user is on the website
    When the user enters username "WrongUsername@test.com"
    And the user enters password "WrongPassword"
    And clicks Login
    Then the user is presented with a error message


  #Refactored: using login function & variables
  Scenario: Login Incorrect credentials refactored
    Given the user is on the website
    When the user logs in with "WrongUsername@test.com" and "WrongPassword"
    Then the user is presented with a error message

  #Refactored: using login function & variables
  Scenario: Login Success refactored
    Given the user is on the website
    When the user logs in with "test@drugdev.com" and "supers3cret"
    Then the user is presented with the "Welcome Dr I Test"

  #Scenario Outline example: This is not the best use case for a SO, however i thought it was be good to show an example me implementing one.
  Scenario Outline: Login tests using Scenario Outline
    Given the user is on the website
    When the user logs in with <username> and <password>
    Then the user is presented with the <message>
    Examples:
      | username                 | password        | message                     |
      | "test@drugdev.com"       | "supers3cret"   | "Welcome Dr I Test"         |
      | "WrongUsername@test.com" | "WrongPassword" | "Credentials are incorrect" |
      | "test@drugdev.com"       | "WrongPassword" | "Credentials are incorrect" |
      | "WrongUsername@test.com" | "supers3cret"   | "Credentials are incorrect" |





