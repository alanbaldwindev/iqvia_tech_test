# Technical Test

## JavaScript

## Question: What changes could you make to the features to make them better for testing?

### Answer: 

I have refactored the given, when, then's to be more efficient using less lines of code when running the login tests. I creating a function for login using variables which can be reused in both correct and incorect scnearios.

An example of this is converting the existing test scenario from:

  Scenario: Login Incorrect credentials
    * Given the user has incorrect credentials
    * When the user enters username 
    * And the user enters password
    * And clicks Login
    * Then the user is presented with an error message 

into:

  Scenario: Login Incorrect credentials
    * Given the user is on the website
    * When the user logs in with "WrongUsername@test.com" and "WrongPassword"
    * Then the user is presented with an error message

*The above example is implemented in this project ;)*

For some other improvements once the application expands, you can add cucumber tags to call specific tests to run in isolation if required and also look at adding before/after functions to set up and tear down processes.

## Question: Are there any features that you think should be added to improve the testing?

### Answer:

**related to the login page:**

It would be good to also add scenarios to the login feature related to entering blank credentials & wrong syntax such as email values consisting of "testdrugdev.com" or "test@drugdev", where we can assert the warning validation is displaying correctly.

example:
  Scenario: Login email validation warning
    * Given the user is on the website
    * When the user logs in with "testdrugdev.com" and "supers3cret"
    * Then the user is presented with a validation warning message "Please include an '@' in the email address."

Ideally we would like to test the whole login page as well. Such as css properties like title value/text, font-size, font-color, background-color, and new features such as login provider, register, fogotten password, page links etc once developed.

**Related to the code/dom:**

*Important*
I would work with the developers to proactively and retrospectively add data-* attributes to elements within the application to provide context to the selectors and insulate them from CSS or JS changes. This is best practice within cypress. Currently, in this login app I had to use the name attribute mostly or access elements with ID or class which are more prone to change and can cause the test cases to fail.

**Running tests:**

I would suggest setting up Continous Integration (CI) for tests to run on builds (nightly or release,  etc.) so tests are automatically run on build completions. If the project grows and ends up with a large number of tests, I would advise running Parallelization on multiple virtual machines to save time when running the tests.

You can also use the *Xray Test Management for Jira* plugin. Xray itself is very good at managing manual and automated tests with some excellent features like a test runner which integrates Xray tests to automated cucumber feature tests. It also has CI integration great reporting features.


## Postman

## Question: Can the test be improved any way?

### Answer

I feel the following test fails due to the application returning the wrong status code for incorrect login

pm.test("Status code is 401", function () {
    pm.response.to.have.status(401);
});

I would have a convocation with one of the developers to investigate the 200 status response on a failed login as I think the response should return 401. If this is proven to be the case, a bug would be raised into development to resolve and correct the status in the API. 

**Collections:**
Its good practice to use Collections to group up API tests, always use meaningful names to save confusion.

**Variables:**
Its good to reuse values so you can keep your code DRY (Don’t Repeat Yourself), In our test case we can use data variables for correct and incorrect credentials to pass into the body. 

**CI integration:**

You can integrate postman into the CI process using Newman.

**API test farmework:**
Consideration: Another option for API testing is SOAP UI. You can build a test framework for the API's which has better test script reusability.

